FROM alpine

RUN apk add --no-cache openssh && \
    sed -i 's/^#GatewayPorts no$/GatewayPorts yes/g' /etc/ssh/sshd_config && \
    sed -i 's/^#ClientAliveInterval 0$/ClientAliveInterval 120/g' /etc/ssh/sshd_config && \
    sed -i 's/^#ClientAliveCountMax 3$/ClientAliveCountMax 720/g' /etc/ssh/sshd_config && \
    sed -i 's/^# Host \*$/Host \*/g' /etc/ssh/ssh_config && \
    echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
    echo "    SendEnv LANG LC_*" >> /etc/ssh/ssh_config && \
    echo "    HashKnownHosts yes" >> /etc/ssh/ssh_config && \
    echo "    UserKnownHostsFile=/dev/null" >> /etc/ssh/ssh_config

COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 22

CMD /usr/sbin/sshd -D
